package user_interface;

import application.main;
import database.xquery;
import static database.xquery.data;
import static database.xquery.tableview;
import java.util.Optional;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class home extends Application {

    public static TextField ub_id = new TextField();
    public static TextField uauthor = new TextField();
    public static TextField utitle = new TextField();
    public static TextField ugenre = new TextField();
    public static TextField uprice = new TextField();
    public static TextField upublished = new TextField();

    public static void startApp() {

        Stage primaryStage = new Stage();

        Button create = new Button();
        create.setText("Add New Book");
        create.setOnAction((ActionEvent event) -> {
            create();
        });

        Button logout = new Button();
        logout.setText("Log Out");
        logout.setOnAction((ActionEvent event) -> {
            primaryStage.close();
            main.login();
            data.clear();
        });

        Button clear = new Button();
        clear.setText("Clear");
        clear.setOnAction((ActionEvent event) -> {
            clear();
        });

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(5, 0, 0, 0));
        hbox.setSpacing(5);
        hbox.getChildren().addAll(create, logout);

        xquery.tableview = new TableView();
        xquery.showData();

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(tableview, hbox);

        StackPane root = new StackPane();
        root.getChildren().addAll(vbox);

        Scene scene = new Scene(root, 680, 455);

        primaryStage.setTitle("Book Collection");
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image("books.png"));
        primaryStage.hide();
        primaryStage.show();

    }

    public static void create() {
        Stage createStage = new Stage();
        StackPane root = new StackPane();
        Scene scene = new Scene(root);

        TextField b_id, author, title, genre, price, published;
        
        b_id = new TextField();
        b_id.setTooltip(new Tooltip("Enter book id"));
        b_id.setPromptText("Book ID [bkxxx]");
        b_id.setMaxWidth(200);

        author = new TextField();
        author.setTooltip(new Tooltip("Author"));
        author.setPromptText("Lastname, Firstname");
        author.setMaxWidth(200);

        title = new TextField();
        title.setTooltip(new Tooltip("Enter Title"));
        title.setPromptText("Title");
        title.setMaxWidth(200);

        genre = new TextField();
        genre.setTooltip(new Tooltip("Enter Genre"));
        genre.setPromptText("Genre");
        genre.setMaxWidth(200);

        price = new TextField();
        price.setTooltip(new Tooltip("Enter Price"));
        price.setPromptText("Price");
        price.setMaxWidth(200);

        published = new TextField();
        published.setTooltip(new Tooltip("Enter Published Date"));
        published.setPromptText("YYYY-MM-DD");
        published.setMaxWidth(200);


        Button savebtn = new Button("Save");
        savebtn.setTooltip(new Tooltip("Save"));

        savebtn.setOnAction(event -> {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Adding new book!");
            alert.setContentText("Do you want to continue?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                xquery insertQ = new xquery();
                insertQ.insertBook(b_id.getText(), author.getText(), title.getText(), genre.getText(), price.getText(), published.getText());
                xquery.refreshData();
                createStage.close();
            }

        });

        VBox vbox = new VBox(10);
        vbox.getChildren().addAll(b_id, author, title, genre, price, published, savebtn);
        vbox.setPadding(new Insets(10));
        root.getChildren().add(vbox);

        createStage.setTitle("New Book");
        createStage.setScene(scene);
        createStage.show();
    }

    public static void update() {

        Stage updateStage = new Stage();
        StackPane root = new StackPane();
        Scene scene = new Scene(root);

        ub_id.setTooltip(new Tooltip("Enter book id"));
        ub_id.setPromptText("Book ID [bkxxx]");
        ub_id.setMaxWidth(200);

        uauthor.setTooltip(new Tooltip("Author"));
        uauthor.setPromptText("Lastname, Firstname");
        uauthor.setMaxWidth(200);

        utitle.setTooltip(new Tooltip("Enter Title"));
        utitle.setPromptText("Title");
        utitle.setMaxWidth(200);

        ugenre.setTooltip(new Tooltip("Enter Genre"));
        ugenre.setPromptText("Genre");
        ugenre.setMaxWidth(200);

        uprice.setTooltip(new Tooltip("Enter Price"));
        uprice.setPromptText("Price");
        uprice.setMaxWidth(200);

        upublished.setTooltip(new Tooltip("Enter Published Date"));
        upublished.setPromptText("YYYY-MM-DD");
        upublished.setMaxWidth(200);

        Button savebtn = new Button("Update");
        savebtn.setTooltip(new Tooltip("Update"));

        savebtn.setOnAction(event -> {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Updating new book!");
            alert.setContentText("Do you want to continue?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                xquery insertQ = new xquery();
                insertQ.updateBook(ub_id.getText(), uauthor.getText(), utitle.getText(), ugenre.getText(), uprice.getText(), upublished.getText());
                xquery.refreshData();
                updateStage.close();
            }

        });

        VBox vbox = new VBox(10);
        vbox.getChildren().addAll(ub_id, uauthor, utitle, ugenre, uprice, upublished, savebtn);
        vbox.setPadding(new Insets(10));
        root.getChildren().add(vbox);

        updateStage.setScene(scene);
        updateStage.show();
    }

    public static void delete(String b_id, String title) {
        
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Deleting book!");
        alert.setContentText("Do you want to delete the book " + title + "?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            xquery.deleteBook(b_id);
            xquery.refreshData();
        }
    }

    public static void clear() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Clearing all entries!");
        alert.setContentText("Do you want to continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            xquery.clear();
            xquery.refreshData();
        }
    }

    public static String getUb_id() {
        return home.ub_id.getText();
    }

    public static void setUb_id(String ub_id) {
        home.ub_id.setText(ub_id);
    }

    public static String getUauthor() {
        return home.uauthor.getText();
    }

    public static void setUauthor(String uauthor) {
        home.uauthor.setText(uauthor);
    }

    public static String getUtitle() {
        return home.utitle.getText();
    }

    public static void setUtitle(String utitle) {
        home.utitle.setText(utitle);
    }

    public static String getUgenre() {
        return home.ugenre.getText();
    }

    public static void setUgenre(String ugenre) {
        home.ugenre.setText(ugenre);
    }

    public static String getUprice() {
        return home.uprice.getText();
    }

    public static void setUprice(String uprice) {
        home.uprice.setText(uprice);
    }

    public static String getUpublished() {
        return home.upublished.getText();
    }

    public static void setUpublished(String upublished) {
        home.upublished.setText(upublished);
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
