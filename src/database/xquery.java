package database;

import com.sun.javafx.scene.control.skin.NestedTableColumnHeader;
import com.sun.javafx.scene.control.skin.TableColumnHeader;
import com.sun.javafx.scene.control.skin.TableHeaderRow;
import com.sun.javafx.scene.control.skin.TableViewSkin;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import user_interface.home;

public class xquery {

    public xquery() {
    }

    public static final ObservableList<ObservableList> data = FXCollections.observableArrayList();
    public static TableView tableview;
    private static TableColumn col;
    public static String getbothvalue;
    public static String b_id;
    public static String author;
    public static String author1;
    public static String title;
    public static String genre;
    public static String price;
    public static String date;

    public void insertBook(String b_id, String author, String title, String genre, String price, String published) {
        Connection c;
        Statement stmt;

        try {
            c = DBConnect.connect();
            System.out.println("Opened database successfully");

            if (!b_id.equals("") && !author.equals("") && !title.equals("") && !genre.equals("") && !price.equals("") && !published.equals("")) {
                stmt = c.createStatement();
                String insertQuery = "update book_records set book_xml.modify('insert <book id = \"" + b_id + "\"><author>" + author
                        + "</author><title>" + title + "</title><genre>" + genre + "</genre>\n" + "<price>" + price + "</price><publish_date>"
                        + published + "</publish_date></book> into (/catalog)[1]')";
                stmt.executeUpdate(insertQuery);

                stmt.close();
                c.close();
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("WARNING!");
                alert.setContentText("Fill up all fields!");

                alert.showAndWait();
            }

        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    public void updateBook(String b_id, String author, String title, String genre, String price, String published) {
        Connection c;
        Statement stmt;

        try {
            c = DBConnect.connect();
            System.out.println("Opened database successfully");

            if (!b_id.equals("") && !author.equals("") && !title.equals("") && !genre.equals("") && !price.equals("") && !published.equals("")) {
                stmt = c.createStatement();
                String updateAuthor = "update book_records set book_xml.modify('replace value of (/catalog[1]/book[@id=\"" + b_id + "\"][1]/author[1]/text()[1]) with (\"" 
					+ author + "\")')";
                stmt.executeUpdate(updateAuthor);

                String updateTitle = "update book_records set book_xml.modify('replace value of (/catalog[1]/book[@id=\"" + b_id + "\"][1]/title[1]/text()[1]) with (\"" 
					+ title + "\")')";
                stmt.executeUpdate(updateTitle);

                String updateGenre = "update book_records set book_xml.modify('replace value of (/catalog[1]/book[@id=\"" + b_id + "\"][1]/genre[1]/text()[1]) with (\"" 
					+ genre + "\")')";
                stmt.executeUpdate(updateGenre);

                String updatePrice = "update book_records set book_xml.modify('replace value of (/catalog[1]/book[@id=\"" + b_id + "\"][1]/price[1]/text()[1]) with (\"" 
					+ price + "\")')";
                stmt.executeUpdate(updatePrice);

                String updateDate = "update book_records set book_xml.modify('replace value of (/catalog[1]/book[@id=\"" + b_id + "\"][1]/publish_date[1]/text()[1]) with (\"" 
					+ published + "\")')";
                stmt.executeUpdate(updateDate);

                stmt.close();
                c.close();
            } else if (b_id.equals("") && author.equals("") && title.equals("") && genre.equals("") && price.equals("") && published.equals("")) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("WARNING!");
                alert.setContentText("Please fill up other fields!");

                alert.showAndWait();
            } else if (b_id.equals("") || author.equals("") || title.equals("") || genre.equals("") || price.equals("") || published.equals("")) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("WARNING!");
                alert.setContentText("A field is empty!");

                alert.showAndWait();
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("WARNING!");
                alert.setContentText("Book ID is empty!");

                alert.showAndWait();
            }
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    public static void deleteBook(String b_id) {

        Connection c;
        Statement stmt;

        try {
            c = DBConnect.connect();
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String deleteBook = "update book_records set book_xml.modify('delete (/catalog/book[@id=\"" + b_id + "\"])[1]')";
            stmt.executeUpdate(deleteBook);

            stmt.close();
            c.close();

        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    public static void showData() {
        Connection c;

        try {
            c = DBConnect.connect();
            //SQL FOR SELECTING ALL OF BOOKS
            String SQL = "select\n"
                    + "    m.c.value('@id', 'nvarchar(max)') as Book_ID,\n"
                    + "    m.c.value('(author/text())[1]', 'nvarchar(max)') as Author,\n"
                    + "    m.c.value('(title/text())[1]', 'nvarchar(max)') as Title,\n"
                    + "    m.c.value('(genre/text())[1]', 'nvarchar(max)') as Genre,\n"
                    + "    m.c.value('(price/text())[1]', 'nvarchar(max)') as Price,\n"
                    + "    m.c.value('(publish_date/text())[1]', 'nvarchar(max)') as Published\n"
                    //+ "    m.c.value('(description/text())[1]', 'nvarchar(max)') as Description\n"
                    + "from book_records as s\n"
                    + "    outer apply s.book_xml.nodes('/catalog/book') as m(c)";
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            /**
             * ********************************
             * TABLE COLUMN ADDED DYNAMICALLY * ********************************
             */
            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //We are using non property style for making dynamic table
                final int j = i;
                col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });

                tableview.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }

            tableview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                    //Check whether item is selected and set value of selected item to Label
                    if (tableview.getSelectionModel().getSelectedItem() != null) {
                        TableView.TableViewSelectionModel selectionModel = tableview.getSelectionModel();
                        ObservableList selectedCells = selectionModel.getSelectedCells();

                        TablePosition tablePosition = (TablePosition) selectedCells.get(0);

                        tablePosition.getTableView().getSelectionModel().getTableView().getId();
                        //gives you selected cell value..
                        Object GetSinglevalue = tablePosition.getTableColumn().getCellData(newValue);

                        getbothvalue = tableview.getSelectionModel().getSelectedItem().toString();
                        //gives you column values..
                        b_id = getbothvalue.split(",")[0].substring(1);
                        author = getbothvalue.split(",")[1].substring(1);
                        author1 = getbothvalue.split(",")[2].substring(1);
                        title = getbothvalue.split(",")[3].substring(1);
                        genre = getbothvalue.split(",")[4].substring(1);
                        price = getbothvalue.split(",")[5].substring(1);
                        date = getbothvalue.split(",")[6].substring(1);
                        date = deleteLastChar(date);
                        //debug purposes
                        System.out.println("The First column value of row.." + b_id);
                        System.out.println("The Second column value of row.." + author);
                        System.out.println("The Third column value of row.." + author1);
                        System.out.println("The Fourth column value of row.." + title);
                        System.out.println("The Fifth column value of row.." + genre);
                        System.out.println("The Sixth column value of row.." + price);
                        System.out.println("The Seventh column value of row.." + date);
                    }
                }
            });

            tableview.setOnMouseClicked((MouseEvent event) -> {
                if (event.getClickCount() > 1) {
                    Stage actionStage = new Stage();
                    StackPane root = new StackPane();
                    Scene scene = new Scene(root);

                    Label actionLabel = new Label("Choose an action:");
                    actionLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 17));

                    Button editbtn = new Button("Edit Book");
                    editbtn.setTooltip(new Tooltip("Edit"));

                    editbtn.setOnAction(e -> {
                        home.setUb_id(b_id);
                        home.ub_id.setText(home.getUb_id());
                        home.setUauthor(author + ", " + author1);
                        home.uauthor.setText(home.getUauthor());
                        home.setUtitle(title);
                        home.utitle.setText(home.getUtitle());
                        home.setUgenre(genre);
                        home.ugenre.setText(home.getUgenre());
                        home.setUprice(price);
                        home.uprice.setText(home.getUprice());
                        home.setUpublished(date);
                        home.upublished.setText(home.getUpublished());
                        
                        actionStage.close();
                        home.update();
                        System.out.println("Clicked!");
                    });

                    Button deletebtn = new Button("Delete Book");
                    deletebtn.setTooltip(new Tooltip("Delete"));

                    deletebtn.setOnAction(e -> {
                        actionStage.close();
                        home.setUb_id(b_id);
                        home.ub_id.setText(home.getUb_id());
                        home.setUtitle(title);
                        
                        home.utitle.setText(home.getUtitle());
                        home.delete(home.getUb_id(), home.getUtitle());
                    });

                    HBox hbox = new HBox();
                    hbox.setPadding(new Insets(5, 0, 0, 0));
                    hbox.setSpacing(5);
                    hbox.getChildren().addAll(editbtn, deletebtn);

                    VBox vbox = new VBox(10);
                    vbox.getChildren().addAll(actionLabel, hbox);
                    vbox.setPadding(new Insets(10));
                    root.getChildren().add(vbox);

                    actionStage.setScene(scene);
                    actionStage.show();
                }
            });

            tableview.skinProperty().addListener((a, b, newSkin) -> {
                TableViewSkin<?> skin = (TableViewSkin<?>) tableview.getSkin();
                TableHeaderRow headerRow = skin.getTableHeaderRow();
                NestedTableColumnHeader rootHeader = headerRow.getRootHeader();
                for (TableColumnHeader columnHeader : rootHeader.getColumnHeaders()) {
                    try {
                        TableColumn<?, ?> column = (TableColumn<?, ?>) columnHeader.getTableColumn();
                        if (column != null) {
                            Method method = skin.getClass().getDeclaredMethod("resizeColumnToFitContent", TableColumn.class, int.class);
                            method.setAccessible(true);
                            method.invoke(skin, column, 30);
                        }
                    } catch (Throwable e) {
                        e = e.getCause();
                        e.printStackTrace(System.err);
                    }
                }
            });

            /**
             * ******************************
             * Data added to ObservableList * ******************************
             */
            while (rs.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);
            }

            //FINALLY ADDED TO TableView
            tableview.setItems(data);
        } catch (SQLException e) {
            System.out.println("Error on Building Data");
        }
    }

//    public void onEdit() {
//        // check the table's selected item and get selected item
//        if (tableview.getSelectionModel().getSelectedItem() != null) {
//            ObservableList selectedPerson = (ObservableList) tableview.getSelectionModel().getSelectedItem();
//            nameTextField.setText(selectedPerson.getName());
//        }
//    }
    public static void refreshData() {
        Connection c;
        data.clear();
        try {
            c = DBConnect.connect();
            //SQL FOR SELECTING ALL OF BOOKS
            String SQL = "select\n"
                    + "    m.c.value('@id', 'nvarchar(max)') as Book_ID,\n"
                    + "    m.c.value('(author/text())[1]', 'nvarchar(max)') as Author,\n"
                    + "    m.c.value('(title/text())[1]', 'nvarchar(max)') as Title,\n"
                    + "    m.c.value('(genre/text())[1]', 'nvarchar(max)') as Genre,\n"
                    + "    m.c.value('(price/text())[1]', 'nvarchar(max)') as Price,\n"
                    + "    m.c.value('(publish_date/text())[1]', 'nvarchar(max)') as Published\n"
                    + "from book_records as s\n"
                    + "    outer apply s.book_xml.nodes('/catalog/book') as m(c)";
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            /**
             * ******************************
             * Data added to ObservableList * ******************************
             */
            while (rs.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            //FINALLY ADDED TO TableView
            tableview.setItems(data);
        } catch (SQLException e) {
            System.out.println("Error on Building Data " + e.getMessage());
        }
    }

    public static void clear() {
        data.clear();
        refreshData();
    }

    public static String deleteLastChar(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ']') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
}
