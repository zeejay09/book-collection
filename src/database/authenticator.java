package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class authenticator {
    public static int UID;
    public static void main(String args[]) {    }

    public static boolean authenticate(String username, String password) {
        Connection c;
        Statement stmt;
        Boolean queryRes = false;
        try {
            c = DBConnect.connect();
            
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT uid, username, password FROM book_admin;");
            while (rs.next()) {
                
                UID = rs.getInt("uid");
                String name = rs.getString("username");
                String pwd = rs.getString("password");

                if (username.equals(name) && password.equals(pwd)) {
                    System.out.println("Success!" + UID);
                    queryRes = true;
                } else {
                    System.out.println("Sorry wrong password and username!");
                }
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
        return queryRes;
    }
}
