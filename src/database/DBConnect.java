package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {

    private static Connection conn;
    private static final String url = "jdbc:sqlserver://localhost:1433;user=master-developer;password=09069604100Z;databaseName=library";
 
    public static Connection connect() throws SQLException {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException cnfe){
            System.err.println("Error: "+cnfe.getMessage());
        }
 
        conn = DriverManager.getConnection(url);
        return conn;
    }
    public static Connection getConnection() {

        return conn;
    }
}
