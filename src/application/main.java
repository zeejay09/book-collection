package application;

import database.authenticator;
import user_interface.login;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import user_interface.home;

public class main extends Application {

    public static void main(String[] args) {
        Application.launch(login.class, args);
    }
    
    public static void login() {
        
        Stage primaryStage = new Stage(); 
        
        primaryStage.setTitle(" User Login");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(50, 25, 25, 25));

        Text scenetitle = new Text("Welcome to Management Console");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 17));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        Button signIn = new Button("Sign in");
        HBox hbsignIn = new HBox(10);
        hbsignIn.setAlignment(Pos.BOTTOM_RIGHT);
        hbsignIn.getChildren().add(signIn);
        grid.add(hbsignIn, 1, 4);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);

        signIn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                final String user = userTextField.getText();
                final String pwd = pwBox.getText();

                final Boolean login_result = authenticator.authenticate(user, pwd);

                if (login_result == true) {

                    home.startApp();
                    primaryStage.hide();
                } else {

                    Stage errorStage = new Stage();
                    GridPane error_dialog = new GridPane();

                    error_dialog.setAlignment(Pos.CENTER);
                    error_dialog.setHgap(10);
                    error_dialog.setVgap(10);
                    error_dialog.setPadding(new Insets(15, 25, 25, 15));

                    Text errorMsg = new Text("Error! Username and Password is not in Database!");
                    errorMsg.setFont(Font.font("Tahoma", FontWeight.NORMAL, 12));
                    error_dialog.add(errorMsg, 1, 1);

                    Button errorOkay = new Button("Okay");
                    HBox hbErrorOkay = new HBox(10);
                    hbErrorOkay.setAlignment(Pos.BOTTOM_CENTER);
                    hbErrorOkay.getChildren().add(errorOkay);
                    error_dialog.add(hbErrorOkay, 1, 2);

                    errorOkay.setOnAction(e -> {
                        errorStage.hide();
                    });

                    Scene errorScene = new Scene(error_dialog, 290, 65);

                    errorStage.setTitle("Warning!");
                    errorStage.setScene(errorScene);

                    errorStage.show();

                    Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
                    errorStage.setX((primScreenBounds.getWidth() - errorStage.getWidth()) / 2);
                    errorStage.setY((primScreenBounds.getHeight() - errorStage.getHeight()) / 2);
                }

            }
        });

        Scene scene = new Scene(grid, 300, 200);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("user.png"));
        primaryStage.setScene(scene);
        primaryStage.show();

        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((primScreenBounds.getWidth() - primaryStage.getWidth()) / 2);
        primaryStage.setY((primScreenBounds.getHeight() - primaryStage.getHeight()) / 2);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
